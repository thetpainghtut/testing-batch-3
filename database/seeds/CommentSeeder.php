<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Comment::class, 10)->create();

        // $comments = factory(App\Comment::class, 10)->create([
        //     'user_id' => $this->getRandomUserId(),
        //     'post_id' => $this->getRandomPostId()
        // ]);
    }

    // private function getRandomUserId() {
    //     $user = App\User::inRandomOrder()->first();
    //     return $user->id;
    // }

    // private function getRandomPostId() {
    //     $post = App\Post::inRandomOrder()->first();
    //     return $post->id;
    // }
}
