<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class, 10)->create();

        // $posts = factory(App\Post::class, 10)->create([
        //     'user_id' => $this->getRandomUserId(),
        //     'category_id' => $this->getRandomCategoryId()
        // ]);
    }

    // private function getRandomUserId() {
    //     $user = App\User::inRandomOrder()->first();
    //     return $user->id;
    // }

    // private function getRandomCategoryId() {
    //     $category = App\Category::inRandomOrder()->first();
    //     return $category->id;
    // }
}
