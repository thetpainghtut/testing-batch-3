<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'category_id' => factory('App\Category')->create()->id,
        'title' => $faker->text($maxNbChars = 191),
        'body' => $faker->text($maxNbChars = 191),
        'photo' => $faker->image('public/storage/images',400,300, null, false)
    ];
});
