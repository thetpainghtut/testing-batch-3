<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'user_id' => factory('App\User')->create()->id,
        'post_id' => factory('App\Post')->create()->id,
        'body' => $faker->text($maxNbChars = 191)
    ];
});
