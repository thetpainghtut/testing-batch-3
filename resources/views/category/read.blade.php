@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
<div class="col-md-8">

    <h1 class="my-4">Category Page
    <small>Secondary Text</small>

    <a href="{{route('category.create')}}" class="btn btn-info float-right">Add Category</a>
    </h1>

    <!-- All Category -->
    <table class="table hovered">
        <thead>
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
        @foreach($allcategories as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td>
                <a href="{{route('category.index')}}" class="btn btn-warning">Edit</a>
                <a href="{{route('category.index')}}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $allcategories->links() }}
</div>
    </div>
</div>

@endsection