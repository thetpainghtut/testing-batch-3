@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
<div class="col-md-8">
    <h1 class="my-4">Create
    <small>New Category</small>
    </h1>
    <!-- Error Msg -->
    
    <!-- Create Page -->

    <form method="post" action="{{route('category.store')}}">
    @csrf
        <div class="form-group">
            <label for="">Category Name:</label>
            <input type="text"
            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="" aria-describedby="helpId" placeholder="" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <input type="submit"
            class="btn btn-info" name="btnsubmit" id="" aria-describedby="helpId" placeholder="" value="Save">
        </div>
    </form>
</div>
    </div>
</div>
@endsection