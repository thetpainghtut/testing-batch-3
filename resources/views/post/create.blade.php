@extends('frontend')
@section('content')
<div class="col-md-8">

    <h1 class="my-4">Upload
    <small>Blog Post</small>
    </h1>
    <!-- Error Msg -->
    
    <!-- Create Page -->

    <form method="post" action="{{route('post.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="">Title:</label>
            <input type="text"
            class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="" aria-describedby="helpId" placeholder="" value="{{ old('title') }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="">Body:</label>
            <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" id="" aria-describedby="helpId" placeholder="" required autofocus>{{ old('body') }}</textarea>
            @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="">Photo:</label>
            <input type="file"
            class="form-control-file{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" id="" aria-describedby="helpId" placeholder="">
            @if ($errors->has('photo'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('photo') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="">Category:</label>
            <select name="category" class="form-control">
            <option value="">Select Category</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
            </select>
            @if ($errors->has('category'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('category') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input type="submit"
            class="btn btn-info" name="btnsubmit" id="" aria-describedby="helpId" placeholder="" value="Save">
        </div>
    </form>

</div>
@endsection