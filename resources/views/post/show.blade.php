@extends('frontend')
@section('content')
<div class="col-md-8">

    <!-- Title -->
    <h1 class="mt-4">{{ $post->title }}</h1>

    <!-- Author -->
    <p class="lead">
    by
    <a href="/post/?user_id={{$post->user_id}}">{{$post->user->name}}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p>
        Posted on {{ $post->created_at->toDayDateTimeString() }}
    </p> 

    @if(Auth::check() && (Auth::id() == $post->user_id))
    <a href="{{route('post.edit',$post->id)}}" class="btn btn-warning btn-sm mr-2">Edit</a>    
    
    <form action="{{ route('post.destroy', $post->id)}}" method="post" class="mt-2">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
    </form>
    @endif

    <hr>

    <!-- Preview Image -->
    <img class="img-fluid rounded" src="{{ $post->photo }}" alt="">

    <hr>

    <!-- Post Content -->
    <p class="lead">{{ $post->body }}</p>

    <hr>

    <!-- Comments Form -->
    <div class="card my-4">
    <h5 class="card-header">Leave a Comment:</h5>
    <div class="card-body">
        
        <input type="hidden" name="postid" value="{{$post->id}}" id="postid">
        <div class="form-group">
            <textarea class="form-control" rows="3" name="comment" id="body"></textarea>
        </div>
        <button type="button" class="btn btn-primary addcomment">Submit</button>
        
    </div>
    </div>

    @foreach($post->comments as $comment)
    <!-- Single Comment -->
    <div class="media mb-4">
    <!-- <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt=""> -->
    <div class="media-body">
        <h5 class="mt-0">{{$comment->user->name}}</h5>
        {{$comment->body}}
        {{$comment->created_at->diffForHumans()}}
    </div>
    </div>
    @endforeach
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            $('.addcomment').on('click',function () {
                var postid = $('#postid').val();
                var body = $('#body').val();
                // alert(postid + body);

                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                  method: 'post',
                  url: '/comment',
                  data: {postid:postid,body:body},
                  success:function (response) {
                      alert(response);
                  }
                })
            })

        })
    </script>
@endsection