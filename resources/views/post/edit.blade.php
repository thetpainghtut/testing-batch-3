@extends('frontend')
@section('content')
<div class="col-md-8">

    <h1 class="my-4">Page Heading
    <small>Secondary Text</small>
    </h1>

    <!-- Create Page -->

    <form method="post" action="{{route('post.update',$post->id)}}" enctype="multipart/form-data">
    @method('PATCH')
    @csrf
        <div class="form-group">
            <label for="">Title:</label>
            <input type="text"
            class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="" aria-describedby="helpId" placeholder="" value="{{ $post->title }}" required autofocus>
            @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="">Body:</label>
            <textarea class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" id="" aria-describedby="helpId" placeholder="" required autofocus>{{ $post->body }}</textarea>
            @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="">Photo:</label>

            <nav>
              <div class="nav nav-tabs">
                <a class="nav-item nav-link active" data-toggle="tab" href="#old">Old</a>
                <a class="nav-item nav-link" data-toggle="tab" href="#new">New</a>
              </div>
            </nav>

            <div class="tab-content mt-3">
              <div class="tab-pane fade show active" id="old">
                <img src="{{$post->photo}}" width="100" height="100">
                <input type="hidden" value="{{$post->photo}}" name="oldphoto">
              </div>

              <div class="tab-pane fade" id="new">
                <input type="file"
                class="form-control-file{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" id="" aria-describedby="helpId" placeholder="">
                @if ($errors->has('photo'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                @endif
              </div>
            </div>  
            
        </div>
        <div class="form-group">
            <label for="">Category:</label>
            <select name="category" class="form-control">
            <option value="">Select Category</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}" @if($post->category_id == $category->id) {{'selected'}} @endif>{{$category->name}}</option>
            @endforeach
            </select>
            @if ($errors->has('category'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('category') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <input type="submit"
            class="btn btn-info" name="btnsubmit" id="" aria-describedby="helpId" placeholder="" value="Save">
        </div>
    </form>

</div>
@endsection