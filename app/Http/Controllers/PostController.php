<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
        // $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'index function of PostController';
        // $posts = Post::all();

        // if ($user_id = request('user_id')) {
        //     $posts = Post::where('user_id', $user_id)
        //        ->orderBy('id', 'desc')
        //        ->get();
        // }

        // if ($category_id = request('category_id')) {
        //     $posts = Post::where('category_id', $category_id)
        //        ->orderBy('id', 'desc')
        //        ->get();
        // }

        // return view('post.read',compact('posts'));
        return view('post.read');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // print_r($request->all());

        $this->validate($request,[
            'title' => 'required|min:2',
            'body' => 'required|min:10',
            'photo' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            'category' => 'required|integer'
        ]);

        if($request->hasfile('photo')){
            $photo = $request->file('photo');
            $name = $photo->getClientOriginalName();
            $photo->move(public_path().'/storage/image/',$name);
            $photo = '/storage/image/'.$name;
        }

        // $post = new Post();
        // $post->title = $request->title;
        // $post->body = $request->body;
        // $post->photo = 'photo';
        // $post->user_id = 1;
        // $post->category_id = $request->category;
        // $post->save();

        Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'photo' => $photo,
            'user_id' => Auth::id(),
            'category_id' => request('category')
        ]);

        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::find($id);
        return view('post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'title' => 'required|min:2',
            'body' => 'required|min:10',
            'oldphoto' => 'required',
            // 'photo' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            'category' => 'required|integer'
        ]);

        if($request->hasfile('photo')){
            $photo = $request->file('photo');
            $name = $photo->getClientOriginalName();
            $photo->move(public_path().'/storage/image/',$name);
            $photo = '/storage/image/'.$name;
        }else{
            $photo = $request->oldphoto;
        }

        $post = Post::find($id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->photo = $photo;
        $post->user_id = Auth::id();
        $post->category_id = $request->category;
        $post->save();

        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        //
        $post = Post::find($id);

        $post->delete();

        return redirect()->route('post.index');
    }
}
