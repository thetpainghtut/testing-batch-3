<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;
class CommentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
    	//validate
    	$this->validate($request,[
    		'postid' => 'required',
    		'body' => 'required|min:10'
    	]);
    	// Insert into comment
    	Comment::create([
    		'post_id' => request('postid'),
    		'user_id' => Auth::id(),
    		'body' => request('body')
    	]);
    	return response()->json(['success'=>'Comment is successfully added']);
    	// return redirect()->back();
    }
}
