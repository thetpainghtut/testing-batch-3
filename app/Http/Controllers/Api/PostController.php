<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PostResource;
use App\Http\Controllers\Api\BaseController as BaseController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Auth;
use Validator;
class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $posts = Post::all();
        // return $posts;
        return PostResource::collection(Post::all());
        // return $this->sendResponse(PostResource::collection(Post::all()),'Posts retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required|min:2',
            'body' => 'required|min:10',
            'photo' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            'category' => 'required|integer'
        ]);


        if($validator->fails()){
            // return $this->sendError('Validation Error.', $validator->errors());
            return $validator->errors();
        }

        if($request->hasfile('photo')){
            $photo = $request->file('photo');
            $name = $photo->getClientOriginalName();
            $photo->move(public_path().'/storage/image/',$name);
            $photo = '/storage/image/'.$name;
        }

        $post = Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'photo' => $photo,
            'user_id' => Auth::id(),
            'category_id' => request('category')
        ]);

        if (is_null($post)) {
            // return $this->sendError('Unauthorized');
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        // return $this->sendResponse(PostResource($post), 'Post created successfully.');
        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::find($id);
        // return $post;
        // if (is_null($post)) {
        //     return $this->sendError('Product not found.');
        // }

        // return $this->sendResponse(PostResource::collection($post));
        return new PostResource($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();


        $validator = Validator::make($input, [
            'title' => 'required|min:2',
            'body' => 'required|min:10',
            'oldphoto' => 'required',
            // 'photo' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
            'category' => 'required|integer'
        ]);


        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
            // return $validator->errors();
        }

        if($request->hasfile('photo')){
            $photo = $request->file('photo');
            $name = $photo->getClientOriginalName();
            $photo->move(public_path().'/storage/image/',$name);
            $photo = '/storage/image/'.$name;
        }else{
            $photo = $request->oldphoto;
        }

        $post = Post::find($id);
        $post->title = request('title');
        $post->body = request('body');
        $post->photo = $photo;
        $post->user_id = Auth::id();
        $post->category_id = request('category');
        $post->save();

        if (is_null($post)) {
            return $this->sendError('Unauthorized');
        }

        // return $this->sendResponse(PostResource($post), 'Post created successfully.');
        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->delete();

        // return $this->sendResponse(PostResource($post), 'Post deleted successfully.');
        return new PostResource($post);
    }
}
