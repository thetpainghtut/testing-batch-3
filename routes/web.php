<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome',['name'=>'mgmg']);
//     // return 'Hello Laravel';
// });

Route::get('/','PostController@index');

Route::resource('/post','PostController');

Route::resource('/category','CategoryController');
Route::post('/comment','CommentController@store')->name('comment');

// Buildin auth routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// For Admin auth
Route::prefix('admin')->group(function () {
    Route::get('dashboard', 'HomeController@index')->name('admin.dashboard');
    Route::get('register', 'AdminController@create')->name('admin.register');
    Route::post('register', 'AdminController@store')->name('admin.register.store');
    Route::get('login', 'AdminController@login')->name('admin.auth.login');
    Route::post('login', 'AdminController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'AdminController@logout')->name('admin.auth.logout');
  });

