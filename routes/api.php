<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
	'prefix' => 'post'
], function (){
	Route::get('/','Api\PostController@index');
	Route::get('/{post}','Api\PostController@show');

	Route::group([
      'middleware' => 'auth:api'
    ], function() {
		// Route::get('/create','Api\PostController@create');
		Route::post('/store','Api\PostController@store');
		// Route::get('/{post}/edit','Api\PostController@edit');
		Route::put('/{post}','Api\PostController@update');
		Route::delete('/{post}','Api\PostController@destroy');
	});
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

